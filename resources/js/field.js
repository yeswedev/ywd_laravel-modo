Nova.booting((Vue, router) => {
    Vue.component('index-ButtonModeration', require('./components/IndexField'));
    Vue.component('detail-ButtonModeration', require('./components/DetailField'));
    Vue.component('form-ButtonModeration', require('./components/FormField'));
    Vue.component('index-ButtonDiff', require('./components/IndexFieldDiff'));
    Vue.component('indicator-moderation', require('./components/IndicatorModeration'));
})
