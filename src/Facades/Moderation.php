<?php

namespace YesWeDev\LaravelModo\Facades;

use Illuminate\Support\Facades\Facade;

class Moderation extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'moderation';
    }
}