<?php
namespace YesWeDev\LaravelModo\Traits;

trait HasRoleModerated
{
    public function hasRoleModerated($roles) {

        if(count($roles) > 0)
        {
            foreach($roles as $role)
            {
                if ($this->roles()->where(config('laravel-modo.role_slug'), $role)->first()) {
                    return true;
                }
            }
        }
        return false;
    }

    public function hasRoleAdmin($role)
    {
        if(isset($this->roles) && 
        ($this->roles->where(config('laravel-modo.role_slug'), $role)->first())){
            return true;
        }
        elseif(isset($this->role) && $this->role()->where(config('laravel-modo.role_slug'), $role)->first())
        {
            return true;
        }
        return false;
    }
}
