<?php
namespace YesWeDev\LaravelModo\Traits;
use YesWeDev\LaravelModo\Moderation;

use Auth;

trait Moderate
{
    /**
     * check if the property is moderatable
     */
    public function isModeratable(string $property) {
        return (isset($this->fieldsModeration[$property]));
    }

    /**
     * set all attributes
     */
    public function setAttribute($property, $value)
    {

        $user = Auth::user();

        if ( $this->isModeratable($property) && isset($this->attributes[$property]) && $user )
        {
            if($this->attributes[$property] != $value)
            {
    
                if($user->hasRoleModerated($this->rolesModeration))
                {
                    $moderation = new Moderation;
                    $moderation->pending($this, $property, $value);
                }
                else
                {
                    if(!is_object($value))
                    {
                        $this->attributes[$property] = $value;
                    }
                }
            }
        }
        else
        {
            if(!is_object($value))
            {
                $this->attributes[$property] = $value;
            }
        }
        
    }
}
