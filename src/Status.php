<?php
namespace YesWeDev\LaravelModo;

class Status
{
    const PENDING = 0;
    const APPROVED = 1;
    const REJECTED = 2;
}