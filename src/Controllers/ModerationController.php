<?php

namespace YesWeDev\LaravelModo\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use YesWeDev\LaravelModo\Moderation;
use YesWeDev\LaravelModo\Diff;

class ModerationController extends Controller
{
    public function approve(Request $request, $id)
    {

        if (request()->expectsJson()) {
            
            $moderation = Moderation::find($id);
            $object = $moderation->type::find($moderation->type_id);
            $moderation->approve($object);

            return $moderation;
        }
    }

    public function reject(Request $request, $id)
    {

        if (request()->expectsJson()) {
            
            $moderation = Moderation::find($id);
            $moderation->reject();

            return $moderation;
        }
    }

    public function diff(Request $request, $id)
    {

        if (request()->expectsJson()) {
            
            $moderation = Moderation::find($id);
            $diff = Diff::compare($moderation->current_value, $moderation->new_value);
            $html = $diff->toTable();

            return $html;
        }
    }

    public function indicator(Request $request)
    {

        if (request()->expectsJson()) {
            
            $moderation = Moderation::get();
            $moderationCount = count($moderation);

            return $moderationCount;
        }
    }
}