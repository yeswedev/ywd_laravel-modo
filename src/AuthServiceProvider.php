<?php

namespace YesWeDev\LaravelModo;

use Illuminate\Support\Facades\Gate;
use YesWeDev\LaravelModo\Moderation;
use YesWeDev\LaravelModo\Policies\ModerationPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Moderation::class => ModerationPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
