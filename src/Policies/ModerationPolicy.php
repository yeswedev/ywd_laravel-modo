<?php

namespace YesWeDev\LaravelModo\Policies;

use App\User;

use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

use YesWeDev\LaravelModo\Traits\HasRoleModerated;

class ModerationPolicy
{
    use HandlesAuthorization, HasRoleModerated;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        if($user->hasRoleAdmin(config('laravel-modo.moderation_role')))
        {
            return true;
        }
    }

    public function view($user, $post)
    {
        return false;
    }

    public function create($user)
    {
        return false;
    }

    public function update($user, $post)
    {
        if($user->hasRoleAdmin(config('laravel-modo.moderation_role')))
        {
            return true;
        }
    }

    public function delete($user, $post)
    {
        if($user->hasRoleAdmin(config('laravel-modo.moderation_role')))
        {
            return true;
        }
    }

    public function restore($user, $post)
    {
        return false;
    }

    public function forceDelete($user, $post)
    {
        return false;
    }
}
