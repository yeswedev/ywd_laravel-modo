<?php

use Illuminate\Support\Facades\Route;

Route::post('api/moderations/approve/{id}', 'YesWeDev\LaravelModo\Controllers\ModerationController@approve');
Route::post('api/moderations/reject/{id}', 'YesWeDev\LaravelModo\Controllers\ModerationController@reject');
Route::get('api/moderations/diff/{id}', 'YesWeDev\LaravelModo\Controllers\ModerationController@diff');
Route::get('api/moderations/indicator', 'YesWeDev\LaravelModo\Controllers\ModerationController@indicator');