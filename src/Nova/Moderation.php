<?php

namespace YesWeDev\LaravelModo\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Nova\Resource;

use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;

use YesWeDev\LaravelModo\Nova\Actions\ModerateAll;

use YesWeDev\LaravelModo\Fields\ButtonModeration;
use YesWeDev\LaravelModo\Fields\ButtonDiff;

class Moderation extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = '\YesWeDev\LaravelModo\Moderation';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'type';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'type',
    ];

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return config('laravel-modo.resources.moderation.group');
    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return config('laravel-modo.moderation_names')['moderation_label'];
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return config('laravel-modo.moderation_names')['moderation_singularlabel'];
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $object = $this->type::find($this->type_id);

        $fields = [

            Text::make(config('laravel-modo.fields.button-moderation.label-type_id'), 'type_id')
                ->displayUsing(function () use ($object) {
                    $properties = config('laravel-modo.properties_name');
                    if(count($properties) > 0)
                    {
                        $results = [];
                        foreach($properties as $property)
                        {
                            if(Schema::hasColumn($object->getTable(), $property))
                            {
                                $results[] = $object->$property;
                            }
                        }
                        return implode(', ', $results);
                    }
                    return '-';
                })
                ->onlyOnIndex(),
            Text::make(config('laravel-modo.fields.button-moderation.label-field'), 'field')
                ->displayUsing(function () use ($object) {
                    $names = config('laravel-modo.field_names');
                    if(count($names) > 0)
                    {
                        foreach($names as $model)
                        {
                            foreach($model as $key => $name)
                            {
                                if($key == $this->field)
                                {
                                    return $name;
                                }
                            }
                        }
                    }
                    return $this->field;
                })
                ->onlyOnIndex(),
            ButtonDiff::make('Compare')->onlyOnIndex(),
            ButtonModeration::make(config('laravel-modo.fields.button-moderation.label-status'),'status')->approve(1)->onlyOnIndex(),
        ];
        if (config('laravel-modo.field_type_id')) {
            $insertFields = [ 
                Text::make(config('laravel-modo.fields.button-moderation.label-type'), 'type')
                ->displayUsing(function () use ($object) {
                    $names = config('laravel-modo.type_names');
                    if(count($names) > 0)
                    {
                        foreach($names as $key => $name)
                        {
                            if($key == class_basename($object)) 
                            {
                                return $name;
                            }
                        }
                    }
                    return class_basename($object);
                })
                ->onlyOnIndex(),
            ];
            array_splice($fields, 2, 0, $insertFields);
        }

        return $fields;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new ModerateAll,
        ];
    }
}