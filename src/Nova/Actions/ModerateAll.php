<?php

namespace YesWeDev\LaravelModo\Nova\Actions;

use YesWeDev\LaravelModo\Moderation;
use YesWeDev\LaravelModo\Fields\ButtonModeration;

use Laravel\Nova\Fields\Text;
use Laravel\Nova\Nova;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ModerateAll extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Get the displayable name of the action.
     *
     * @return string
     */
    public function name()
    {
        return config('laravel-modo.action_name') ?: Nova::humanize($this);
    }

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $model)
        {

            if($fields->status == 'approve')
            {
                $object = $model->type::find($model->type_id);
                $model->approve($object);
                $model->delete();
                return Action::redirect('moderations');
            }
            else
            {
                $model->reject();
                $model->delete();
                return Action::redirect('moderations');
            }
            
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            ButtonModeration::make(config('laravel-modo.fields.button-moderation.label-status'),'status')
                ->options([
                    'approve' => config('laravel-modo.approve'),
                    'decline' => config('laravel-modo.decline'),
                ])
                ->approve(1),
        ];
    }
}
