<?php

namespace YesWeDev\LaravelModo\Fields;

use Laravel\Nova\Fields\Field;

class ButtonDiff extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'ButtonDiff';
}
