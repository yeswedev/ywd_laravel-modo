<?php

namespace YesWeDev\LaravelModo\Fields;

use Laravel\Nova\Fields\Field;

class ButtonModeration extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'ButtonModeration';

    /**
     * Set the approved value.
     *
     * @param  int  $value
     * @return $this
     */
    public function approve(int $value = 1)
    {
        return $this->withMeta(['approve' => $value]);
    }

    /**
     * Specify the available options
     *
     * @param array $options
     * @return self
     */
    public function options(array $options)
    {
        return $this->withMeta(['options' => $options]);
    }
}
