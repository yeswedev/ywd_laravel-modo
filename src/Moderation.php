<?php

namespace YesWeDev\LaravelModo;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Moderation extends Model
{
    protected $table = 'moderations';

    protected $fillable = [
        'type',
        'type_id',
        'field',
        'status',
        'moderated_by',
    ];

    public function approve($object)
    {

        $field = $this->field;

        $this->moderated_at = Carbon::now();
        $this->status = Status::APPROVED;
        $this->save();

        $object->$field = $this->new_value;
        $object->save();
    }

    public function reject()
    {

        $this->moderated_at = Carbon::now();
        $this->status = Status::REJECTED;
        $this->save();
    }

    public function pending($object, $field, $value)
    {

        $this->type = 'App\\'.class_basename($object);
        $this->type_id = $object->id;
        $this->field = $field;
        $this->current_value = $object->$field;
        $this->new_value = $value;
        $this->status = Status::PENDING;
        $this->save();
    }
}
