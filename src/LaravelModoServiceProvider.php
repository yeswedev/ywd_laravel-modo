<?php

namespace YesWeDev\LaravelModo;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Nova;
use Laravel\Nova\Events\ServingNova;

use YesWeDev\LaravelModo\Nova\Moderation;

class LaravelModoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/laravel-modo.php' => config_path('laravel-modo.php'),
        ], 'laravel-modo');

        $this->publishes([
            __DIR__.'/../resources/views/resources' => resource_path('views/vendor/nova/resources'),
        ], 'laravel-modo');

        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

        Nova::serving(function (ServingNova $event) {
            Nova::script('laravel-modo', __DIR__.'/../dist/js/field.js');
            Nova::style('laravel-modo', __DIR__.'/../dist/css/field.css');
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/laravel-modo.php',
            'laravel-modo'
        );

        $this->app->bind('moderation', function () {
            return new Moderation;
        });

        Nova::resources([
            Moderation::class,
        ]);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['moderation'];
    }
}
