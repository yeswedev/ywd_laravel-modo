# YWD Laravel Nova Field Moderation

A simple Moderation.

This package allows you to approve or reject resources for each fields you configure

![screenshot](https://framagit.org/yeswedev/ywd_laravel-modo/raw/master/screenshot.jpg)

## Installation

Install this package in your Laravel app via composer:

```bash
composer require yeswedev/laravel-modo
```

You need to add this to your `config/app.php` providers array

```php

'providers' => [
    ...
    YesWeDev\LaravelModo\LaravelModoServiceProvider::class,

];
```

Publish the config file

```bash
php artisan vendor:publish --tag=laravel-modo
```

And finaly migrate the moderations table into your database

```bash
php artisan migrate
```

or

```bash
php artisan migrate --path=vendor/yeswedev/laravel-modo/database/migrations

```

## Preparation

First you need to prepare your model with the `Moderate` trait

```php

use YesWeDev\LaravelModo\Traits\Moderate;

class YourModel extends Model
{
    use Moderate;
    ...
}
```

You need to add the second trait `HasRoleModerated` to your `User` model

```php

use YesWeDev\LaravelModo\Traits\HasRoleModerated;

class User extends Model
{
    use HasRoleModerated;
    ...
}
```

Now you are ready to use it !

## Usage

In your model add two array `$rolesModeration` and `$fieldsModeration`

The `$rolesModeration` array must contain your desired user role which can be moderated by your admin

The `$fieldsModeration` contains the fields which can be moderated by your admin

```php

use YesWeDev\LaravelModo\Traits\Moderate;

class YourModel extends Model
{
    use Moderate;
    
    protected $rolesModeration = [
        'moderator'
    ];

    protected $fieldsModeration = [
        'title' => 'title',
        'description' => 'description',
    ];
}
```

You can set your user role which can moderate in `config/laravel-modo`, default is `admin`

```php

/*
|--------------------------------------------------------------------------
| Moderator Role
|--------------------------------------------------------------------------
|
| Define the role which can moderate.
|
*/
'moderation_role' => 'admin',
...
```


## License

The BSD 2 Clause License (BSD 2).