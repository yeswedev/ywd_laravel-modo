<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModerationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moderations', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('type')->nullable();
            $table->integer('type_id')->unsigned()->nullable();

            $table->string('field')->nullable();
            $table->text('current_value')->nullable();
            $table->text('new_value')->nullable();
            $table->integer('status')->nullable();

            $table->dateTime('moderated_at')->nullable();
            
            $table->integer('moderated_by')->unsigned()->nullable();
            $table->foreign('moderated_by')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moderations');
    }
}
