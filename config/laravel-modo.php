<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Moderator Role
    |--------------------------------------------------------------------------
    |
    | Define the role which can be moderate.
    |
    */
    'moderation_role' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | Role Column
    |--------------------------------------------------------------------------
    |
    | Define the role column name or slug which to be use. default is 'name'
    |
    */
    'role_slug' => 'name',

    /*
    |--------------------------------------------------------------------------
    | Lang Action Moderations
    |--------------------------------------------------------------------------
    |
    | Define the role which can moderate.
    |
    */
    'action_name' => 'Moderate',
    'approve' => 'Approve',
    'decline' => 'Decline',

    /*
    |--------------------------------------------------------------------------
    | Field 'Field Name'
    |--------------------------------------------------------------------------
    |
    | Define the name which can be show instead of the column name.
    | Set the key as property and change the value to be display using
    |
    | Example: 
    |
    | 'field_names' => [
    |    'YourModel' => [
    |        'title' => 'title',
    |    ]
    | ],
    |
    */
    'field_names' => [
        
    ],

    /*
    |--------------------------------------------------------------------------
    | Field Type (Model)
    |--------------------------------------------------------------------------
    |
    | Define the name which can be show instead of the class name.
    | Set the key as model and change the value to be display using
    |
    | Example: 
    |
    | 'type_names' => [
    |    'YourModel' => 'Value'
    | ],
    |
    */
    'type_names' => [
        
    ],

    /*
    |--------------------------------------------------------------------------
    | Field Type Name
    |--------------------------------------------------------------------------
    |
    | Define the property which can be show instead of id.
    |
    | Example: 
    |
    | 'properties_name' => [
    |    'title' => 'title',
    | ],
    |
    */
    'properties_name' => [
        
    ],

    /*
    |--------------------------------------------------------------------------
    | Fields Display
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default display configuration of fields.
    |
    */
    'field_type_id' => true,

    /*
    |--------------------------------------------------------------------------
    | Resources Names
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default names of resources.
    |
    */
    'moderation_names' => [
        'moderation_label' => 'Moderations',
        'moderation_singularlabel' => 'Moderation',
    ],

    /*
    |--------------------------------------------------------------------------
    | Resources Group Names
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default group names of resources.
    |
    */
    'resources' => [
        'moderation' => [
            'group' => 'Other',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Field Label
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default label of field.
    |
    */
    'fields' => [
        'button-moderation' => [
            'label-field'  => 'Field Name',
            'label-type'  => 'Type Name',
            'label-type_id'  => 'Type Id',
            'label-current_value'  => 'Current Value',
            'label-new_value'  => 'New Value',
            'label-status'  => 'Action',
        ],
    ],
];
